﻿using DesafioHackathon.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DesafioHackathon.Controllers
{
    public class CartaController : Controller
    {
        private readonly CartaContext _context;

        public CartaController(CartaContext context)
        {
            _context = context;
        }


        private readonly string _Url = string.Format("https://localhost:44309");
        private readonly string _UrlApi = string.Format("/api/CartaService/");

        // GET: Carta
        public async Task<ActionResult> Index()
        {
            return View(await _context.Carta.ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var carta = await _context.Carta
                .SingleOrDefaultAsync(m => m.Id == id);
            if (carta== null)
            {
                return NotFound();
            }

            return View(carta);
        }

        // GET: Contatos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cartas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nome,Numero,Endere,Dest,Remet,Mensagem")] Carta carta
            )
        {
            if (ModelState.IsValid)
            {
                _context.Add(carta);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(carta);
        }

        

        // GET: Carta/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Carta/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Carta/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Carta/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}